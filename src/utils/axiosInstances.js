import axios from 'axios';

export const axiosInstance = axios.create({
  baseURL: 'http://localhost:8000/',
});

export const authenticatedAxios = axios.create({
  baseURL: 'http://localhost:8000/',
  headers: {Authorization: 'Token ' + localStorage.getItem('token')},
});