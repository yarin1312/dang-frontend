export const emailValidation = (val) => {
  if(val && ((val.indexOf('@') === -1 || val.indexOf('.') === -1) || (val.indexOf('@') > val.lastIndexOf('.')) || val.lastIndexOf('.') === val.length -1)) {
    return {error: true, error_msg: "Invalid email address"}
  }
  return {error: false, error_msg: ""}
}

export const passwordValidation = (val) => {
  if (val && val.length < 8){
    return {error: true, error_msg: "Password must contain at least 8 characters"}
  }
  return {error: false, error_msg: ""}
}

export const confirmPasswordValidation = password1 => password2 => {
  if (password2 && password1 !== password2) {
    return {error: true, error_msg: "Confirmation failed, please make sure the password are equal"}
  }
  return {error: false, error_msg: ""}
}

export const phoneNumberValidation = (val) => {
  const express = new RegExp('^[0-9]*$')
  if (val && !express.test(val)) {
    return {error: true, error_msg: "Only digits allow"}
  }
  return {error: false, error_msg: ""}
}

export const digitsOnlyNoZero = (val) => {
  const express = new RegExp('^[0-9]*$')
  if (val && !express.test(val)) {
    return {error: true, error_msg: "Only digits allow"}
  }
  if (val !== "" && !parseInt(val)) {
    return {error: true, error_msg: "Value can't be zero"}
  }
  return {error: false, error_msg: ""}
}
