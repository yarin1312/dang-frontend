import moment from 'moment';

export const calculateTurns = (turnMinutes) => {
  let now = moment('2013-01-01T00:00');
  const tommorow = moment('2013-01-02T00:00');
  let turnsTime = []

  while (now.isBefore(tommorow)) {
    turnsTime.push(now.format("HH:mm"));
    now.add(turnMinutes, 'm');
  }
  return turnsTime;
}

export const calculateTurnsFromTime = (turnMinutes, startTime, removeFirst) => {
  let now = moment("2013-01-01T" + startTime);
  if (removeFirst) {
    now.add(turnMinutes, 'm');
  }
  const tommorow = moment('2013-01-02T00:00');

  let turnsTime = []
  while (now.isBefore(tommorow)) {
    turnsTime.push(now.format("HH:mm"));
    now.add(turnMinutes, 'm');
  }
  return turnsTime;
}

export const dayOfWeekToString = {
  6: "Sunday",
  0: "Monday",
  1: "Tuesday",
  2: "Wednesday",
  3: "Thrsday",
  4: "Friday",
  5: "Saturday"
}