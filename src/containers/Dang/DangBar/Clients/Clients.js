import React, {Component} from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';

import {authenticatedAxios} from '../../../../utils/axiosInstances';
import CreateClient from './CreateClient/CreateClient';
import SingleClient from './SingleClient/SingleClient';

const styles = theme => ({
  header: {
    display: "inline",
  },
  grow: {
    flexGrow: 1,
  },
  toolbar: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  button: {
    margin: theme.spacing.unit * 2,
  },
  clientFrame: {
    marginTop: theme.spacing.unit,
  }
});

class Clients extends Component {
  classes = this.props.classes
  state = {
    clients: [],
    expandedClient: "",
    createClient: false,
  }
  handleChange = panel => (event, expanded) => {
    this.setState({expandedClient: expanded ? panel : ""})
  }
  componentDidMount = () => {
    authenticatedAxios.get('api/internal/clients/')
      .then( (res) => {
        this.setState({clients: res.data.results})
      })
      .catch( (err) => console.log(err))
  }
  deleteClient = clientToDelete => () => {
    authenticatedAxios.delete('api/internal/clients/' + clientToDelete.id + '/')
      .then( (res) => {
        const updatedClients = this.state.clients.filter( client => client.id !== clientToDelete.id);
        this.setState({clients: updatedClients, expandedClient: ""});
      })
      .catch( (err) => console.log(err))
  }
  render () {
    return (
      <div>
        <div className={this.classes.toolbar}>
          <h2 className={this.classes.header} style={{paddingLeft: "10px"}}>Clients</h2>
          <div className={this.classes.grow} />
          <Button variant="outlined"
                  className={this.classes.button}
                  color="primary"
                  onClick={() => this.setState({createClient: true})}
                  size="small">Add Client</Button>
        </div>
        <Divider />
        <div className={this.classes.clientFrame}>
          {this.state.clients.map( (client, index) => 
            <SingleClient handleChange={this.handleChange('client' + index)}
                          isExpanded={this.state.expandedClient === 'client' + index}
                          client={client}
                          key={index}
                          handleDelete={this.deleteClient(client)} />
          )}
        </div>
        <CreateClient open={this.state.createClient}
                      handleClose={() => this.setState({createClient: false})}/>
      </div>
    )
  }
}

Clients.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Clients);