import React, {Component} from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Slide from '@material-ui/core/Slide';

import DangTextField from '../../../../../components/DangFields/DangTextField';
import DangPasswordField from '../../../../../components/DangFields/DangPasswordField';
import {emailValidation,
        passwordValidation,
        phoneNumberValidation} from '../../../../../utils/validators';
import { authenticatedAxios } from '../../../../../utils/axiosInstances';

const Transition = props => {
  return <Slide direction="up" {...props} />;
}

const styles = theme => ({
  appBar: {
    position: 'relative',
  },
  flex: {
    flex: 1,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
    backgroundColor: "white"
  },
  submit: {
    margin: theme.spacing.unit,
  }
});

class CreateClient extends Component {
  classes = this.props.classes
  state = {
    email: {value: "", error: false},
    password: {value: "", error: false},
    full_name: {value: "", error: false},
    contact_phone_number: {value: "", error: false},
    errorString: ""
  }
  handleChange = (name) => (value, error) => {
    let currentEntity = {...this.state[name]}
    currentEntity.value = value;
    currentEntity.error = error;

    this.setState({[name]: currentEntity});
  }
  createNewClient = () => {
    authenticatedAxios.post("api/internal/clients/",
      {
        email: this.state.email.value,
        password: this.state.password.value,
        full_name: this.state.full_name.value,
        contact_phone_number: this.state.contact_phone_number.value
      })
    .then(res => console.log(res.data))
    .catch(err => console.log(err.response.data))
  }
  render () {
    const errors = this.state.email.error || this.state.password.error || this.state.full_name.error;
    return (
      <Dialog open={this.props.open}
              onClose={this.props.handleClose}
              aria-labelledby="form-title">
        <DialogTitle id="form-title">Create New Client</DialogTitle>
        <div>
          <DialogContent>
            <DialogContentText>Create new client for your business, once you created the client he could login</DialogContentText>
            <DangTextField type="email"
                          value={this.state.email.value}
                          handleChange={this.handleChange('email')}
                          label="Email"
                          className={this.classes.textField}
                          validation={emailValidation}
                          required/>
            <DangPasswordField required
                              label="Password"
                              validation={passwordValidation}
                              value={this.state.password.value}
                              handleChange={this.handleChange("password")}
                              className={this.classes.textField} />
            <DangTextField required
                          label="Full Name"
                          value={this.state.full_name.value}
                          handleChange={this.handleChange("full_name")}
                          className={this.classes.textField}/>
            <DangTextField type="tel"
                          label="Contact phone number"
                          value={this.state.contact_phone_number.value}
                          handleChange={this.handleChange('contact_phone_number')}
                          className={this.classes.textField}
                          validation={phoneNumberValidation}/>
          </DialogContent>
          <DialogActions>
            <Button color="secondary"
                    onClick={this.props.handleClose}>Cancel</Button>
            <Button color="primary"
                    onClick={this.createNewClient}
                    disabled={errors}>Add Client</Button>
          </DialogActions>
        </div>
      </Dialog>
    );
  }
}

CreateClient.propTypes = {
  classes: PropTypes.object.isRequired,
};


export default withStyles(styles)(CreateClient);