import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const styles = theme => ({
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
  button: {
    marginRight: theme.spacing.unit,
  },
  grow: {
    flexGrow: 1,
  },
  boldText: {
    fontWeight: "bold"
  },
  property: {
    margin: theme.spacing.unit,
  }
});

const singleClient = props => {
  const classes = props.classes
  return (
    <ExpansionPanel expanded={props.isExpanded} onChange={props.handleChange}>
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
      <Typography className={classes.heading}>{props.client.full_name}</Typography>
      <Typography className={classes.secondaryHeading}>{props.client.email}</Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        <div style={{display: "block"}}>
          <div className={classes.property}>
            <span className={classes.boldText}>Full Name:</span> {props.client.full_name}
          </div>
          <div className={classes.property}>
            <span className={classes.boldText}>Phone Number:</span> {props.client.contact_phone_number}
          </div>
          <div className={classes.property}>
            <span className={classes.boldText}>Email:</span> {props.client.email}
          </div>
          <Button color="secondary"
                  className={classes.property}
                  variant="contained"
                  onClick={props.handleDelete}>Remove Client</Button>
        </div>
      </ExpansionPanelDetails>
    </ExpansionPanel>
  )
}

export default withStyles(styles)(singleClient);