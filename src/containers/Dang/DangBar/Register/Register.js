import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

import DangTextField from '../../../../components/DangFields/DangTextField';
import DangPasswordField from '../../../../components/DangFields/DangPasswordField';
import {emailValidation,
        passwordValidation,
        phoneNumberValidation} from '../../../../utils/validators';

const styles = theme => ({
  container: {
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 300,
    backgroundColor: "white"
  },
  dense: {
    marginTop: 19,
  },
  menu: {
    width: 200,
  },
  heading: {
    fontWeight: "bold",
  }
});

class Register extends Component {
  classes = this.props.classes;
  state = {
    user: {
      email: {value: "", error: false},
      password: {value: "", error: false},
      password2: {value: "", error: false},
      full_name: {value: "", error: false},
      contact_phone_number: {value: "", error: false}
    },
    business: {
      business_name: {value: "", error: false},
      business_host: {value: "", error: false},
      business_timezone: {value: "", error: false}
    }
  }

  handleChange = (entity, name) => (value, error) => {
    let currentEntity = {...this.state[entity]}
    currentEntity[name].value = value;
    currentEntity[name].error = error;

    this.setState({[entity]: currentEntity});
  }
  
  getValues = (jsonObj) => {
    let parsedJson = {}
    const jsonKeys = Object.keys(jsonObj);
    for (let i in jsonKeys) {
      parsedJson[jsonKeys[i]] = jsonObj[jsonKeys[i]].value;
    }

    return parsedJson;
  }

  register = (formEvent) => {
    formEvent.preventDefault();
    let dataToSend = this.getValues(this.state.user);
    const business = this.getValues(this.state.business);
    dataToSend.business = business;

    axios.post('api/internal/register_business', dataToSend)
      .then( (res) => {
        this.props.onUserLogin({token: res.data.token, user: res.data.user});
        localStorage.setItem('user', JSON.stringify(res.data.user));
        localStorage.setItem('token', res.data.token);

        window.location = window.location.origin;
      })
      .catch( (err) => {
        console.log(err);
      })
  }

  checkErrors = () => {
    const userValues = Object.values(this.state.user);
    const businessValues = Object.values(this.state.business);
    for (let i in userValues) {
      if (userValues[i].error){
        return true;
      }
    }
    for (let i in businessValues) {
      if (businessValues[i].error){
        return true;
      }
    }
    return false;
  }

  render() {
    return (
      <div>
        <form className={this.classes.container} autoComplete="off" onSubmit={this.register}>
          <div>
            <div className={this.classes.heading} >
              <h3>Personal Details</h3>
            </div>
          </div>
          <DangTextField type="email"
                         value={this.state.user.email.value}
                         handleChange={this.handleChange('user', 'email')}
                         label="Email"
                         className={this.classes.textField}
                         validation={emailValidation}
                         required/>
          <DangPasswordField required
                         label="Password"
                         validation={passwordValidation}
                         value={this.state.user.password.value}
                         handleChange={this.handleChange("user", "password")}
                         className={this.classes.textField} />
                        {/* //  type="password"/> */}
          {/* <DangTextField required
                         value={this.state.user.password2.value}
                         handleChange={this.handleChange("user", "password2")}
                         label="Password Confirmation"
                         className={this.classes.textField}
                         type="password"
                         validation={confirmPasswordValidation(this.state.user.password1.value)}/> */}
          <DangTextField required
                         label="Full Name"
                         value={this.state.user.full_name.value}
                         handleChange={this.handleChange("user", "full_name")}
                         className={this.classes.textField}/>
          <div style={{marginTop: "20px"}}>
            <div className={this.classes.heading}>
              <h3>Tell us about you'r business</h3>
            </div>
            <DangTextField required
                           label="Business Name"
                           className={this.classes.textField}
                           value={this.state.business.business_name.value}
                           handleChange={this.handleChange('business', 'business_name')}/>
            <DangTextField label="Business Host"
                           className={this.classes.textField}
                           value={this.state.business.business_host.value}
                           handleChange={this.handleChange('business', 'business_host')}
                           helperText="Please enter your business's host to allow api request (can be defined later)"/>
            <TextField id="country"
                       select
                       label="Country"
                       className={this.classes.textField}
                       onChange={this.handleChange('business', 'business_timezone')}
                       value="test"
                       SelectProps={{
                        native: true,
                        MenuProps: {
                          className: this.classes.menu,
                         },
                       }}
                       helperText="Please select your Business's conutry"
                       margin="normal"
                       variant="outlined">
              <option value="test">Select Country</option>
            </TextField>
            <DangTextField type="tel"
                           label="Contact phone number"
                           value={this.state.user.contact_phone_number.value}
                           handleChange={this.handleChange('user', 'contact_phone_number')}
                           className={this.classes.textField}
                           validation={phoneNumberValidation}/>
          </div>
          <Button type="submit"
                  variant="contained"
                  color="primary"
                  fullWidth
                  disabled={this.checkErrors()}
                  style={{marginTop: "30px"}}
                  size="large">Join Dang</Button>
        </form>
      </div>
    )
  }
}

Register.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapDispatchToProps = dispatch => {
  return {
    onUserLogin: () => dispatch({type: "LOGIN"})
  };
};

export default connect(null, mapDispatchToProps)(withStyles(styles)(Register));