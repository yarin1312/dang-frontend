import React, {Component} from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import PropTypes from 'prop-types';

import {withStyles} from '@material-ui/core/styles';
import ArrowBack from '@material-ui/icons/ArrowBack';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import FormControl from '@material-ui/core/FormControl';
import IconButton from '@material-ui/core/IconButton';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

import {authenticatedAxios} from '../../../../utils/axiosInstances';
import DisplaySingleTurn from './DisplaySingleTurn/DisplaySingleTurn';

const styles = theme => ({
  toolbar: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  grow: {
    flexGrow: 1,
  },
  button: {
    margin: theme.spacing.unit,
    fontWeight: "bold",
  },
});

class DisplaySingleWorker extends Component {
  classes = this.props.classes
  state = {
    worker: null,
    dateToDisplay: moment().format('YYYY-MM-DD'),
    turnsToDisplay: [],
    expandedTurn: "",
    createSpecial: false
  }
  componentDidMount = () => {
    if (this.props.worker) {
      this.setState({worker: this.props.worker})
    } else {
      this.initWorker();
    }
  }
  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if (this.state.worker && parseInt(this.props.match.params.workerId) !== this.state.worker.id){
      this.initWorker(); 
    }
  }
  initWorker = () => {
    authenticatedAxios.get('api/internal/worker/' + this.props.match.params.workerId + '/')
      .then( (res) => {
        const worker = res.data
        this.setState({worker: worker});
        this.props.onSaveWorker(worker);
        authenticatedAxios.get('api/internal/turn/?worker=' + worker.id + '&turn_date=' + this.state.dateToDisplay)
          .then((res) => this.setState({turnsToDisplay: res.data.results}))
          .catch( (err) => console.log(err))
      })
      .catch( (err) => console.log(err))
  }
  getDatesOptions = () => {
    let currentTime = moment();
    let range = this.state.worker.turns_create_range;
    let options = [
      <MenuItem key={0} value={currentTime.format("YYYY-MM-DD")}>
        {currentTime.format("YYYY-MM-DD")}
      </MenuItem>
    ]

    for (var i = 1; i < range; i++) {
      currentTime.add(1, 'd')
      options.push(
        <MenuItem key={i} value={currentTime.format("YYYY-MM-DD")}>
          {currentTime.format("YYYY-MM-DD")}
        </MenuItem>
      );
    }

    return options
  }
  handleChange = panel => (event, expanded) => {
    this.setState({expandedTurn: expanded ? panel : ""})
  }
  handleChangeDate = event => {
    const newDate = event.target.value;
    authenticatedAxios.get('api/internal/turn/?worker=' + this.state.worker.id + '&turn_date=' + newDate)
      .then( (res) => this.setState({dateToDisplay: newDate, turnsToDisplay: res.data.results}))
      .catch( (err) => console.log(err))
  }
  render () {
    if (this.state.worker) {
      return (
        <div>
          <div>
            <div className={this.classes.toolbar}>
              <div>
                <h2>
                <IconButton onClick={() => this.props.history.push('/my_workers/')}>
                  <ArrowBack />
                </IconButton>
                  {this.state.worker.worker_name}
                </h2>
                <small>
                  <span style={{fontWeight: "bold"}}>Turns creataion range:</span>
                  <span>{this.state.worker.turns_creation_range} days</span>
                </small>
                </div>
              <div className={this.classes.grow} />
              <div style={{marginTop: "auto"}}>
              <Button variant="contained"
                      size="small"
                      className={this.classes.button}
                      onClick={() => this.props.history.push('/worker/' + this.state.worker.id + '/settings')}>
                Edit Worker
              </Button>
            </div>
            </div>
            <Divider />
            <div className={this.classes.toolbar}>
              <FormControl className={this.classes.formControl}>
                <InputLabel htmlFor="date-to-display">Date</InputLabel>
                <Select value={this.state.dateToDisplay}
                        onChange={this.handleChangeDate}
                        inputProps={{
                          name: 'dateToDisplay',
                          id: 'date-to-display'
                        }}>
                  {this.getDatesOptions()}
                </Select>
              </FormControl>
              <div className={this.classes.grow} />
              <Button color="primary"
                      size="small"
                      variant="outlined"
                      className={this.classes.button}
                      onClick={() => this.props.history.push('/worker/' + this.state.worker.id + '/specials')}>One-Time WorkDays</Button>
            </div>
          </div>
          <Divider />
          <div>
            {this.state.turnsToDisplay.map( (turn, index) => 
              <DisplaySingleTurn handleChange={this.handleChange('turn' + index)}
                                 isExpanded={this.state.expandedTurn === 'turn' + index}
                                 turn={turn}
                                 key={index} />)}
          </div>
        </div>
      )
    }
    return (
      <div>Technical Issues</div>
    )
  }
}

DisplaySingleWorker.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapDispatchToProps = dispatch => {
  return {
    onSaveWorker: (worker) => dispatch({
      type: "SAVE_WORKER",
      worker: worker
    })
  };
};

const mapStateToProps = state => {
  return {
    worker: state.worker
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(DisplaySingleWorker));