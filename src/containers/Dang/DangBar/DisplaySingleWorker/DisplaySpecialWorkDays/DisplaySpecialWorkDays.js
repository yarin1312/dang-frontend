import React, {Component} from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import ArrowBack from '@material-ui/icons/ArrowBack';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';

import {authenticatedAxios} from '../../../../../utils/axiosInstances';
import CreateSpecialWorkDay from '../CreateSpecialWorkDay/CreateSpecialWorkDay';
import DisplayWorkDays from '../../Workers/CreateWorker/DisplayWorkDays/DisplayWorkDays';

const styles = theme => ({
  toolbar: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  grow: {
    flexGrow: 1,
  },
  button: {
    margin: theme.spacing.unit,
    fontWeight: "bold",
  },
});

class DisplaySpecials extends Component {
  classes = this.props.classes
  state = {
    worker: null,
    specialWorkDays: [],
    createSpecial: false,
  }
  componentDidMount = () => {
    if (!this.props.worker) {
      const workerId = this.props.match.params.workerId;
      authenticatedAxios.get('api/internal/worker/' + workerId + '/')
        .then( (res) => {
          this.setState({
            worker: res.data,
            specialWorkDays: this.extractSpecialWorkDays(res.data.workdays)
          });
          this.props.onSaveWorker(res.data);
        })
        .catch( (err) => console.log(err))
    } else {
      this.setState({
        worker: this.props.worker,
        specialWorkDays: this.extractSpecialWorkDays(this.props.worker.workdays)
      })
    }
  }
  extractSpecialWorkDays = (allWorkDays) => {
    return allWorkDays.filter( workday => workday.type === 'special_work_day');
  }
  handleDeleteWorkDay = workdayToRemove => () => {
    authenticatedAxios.delete('api/internal/workday/' + workdayToRemove.id + '/')
      .then( (res) => {
        const workdays = this.state.worker.workdays.filter( workday => workday.id !== workdayToRemove.id);
        const updatedWorker = this.state.worker;
        updatedWorker.workdays = workdays;

        this.setState({
          specialWorkDays: this.extractSpecialWorkDays(workdays),
          worker: updatedWorker
        });
        this.props.onSaveWorker(updatedWorker);
      })
  }
  createSpecial = (specialWorkDay) => {
    let workdays = [...this.state.worker.workdays];
    workdays.push(specialWorkDay);
    let worker = {...this.state.worker}
    worker.workdays = workdays;

    this.setState({
      worker: worker,
      specialWorkDays: this.extractSpecialWorkDays(workdays)
    });
    this.props.onSaveWorker(worker);
  }
  render () {
    if (this.state.worker) {
      return (
        <div>
          <div className={this.classes.toolbar}>
            <h2 style={{paddingLeft: "10px"}}>
              <IconButton onClick={() => this.props.history.push('/worker/' + this.state.worker.id)}>
                <ArrowBack />
              </IconButton>
              One-Time Work Days 
            </h2>
            <div className={this.classes.grow} />
            <Button color="primary"
                    variant="outlined"
                    onClick={() => this.setState({createSpecial: true})}
                    size="small"
                    className={this.classes.button}>Create New Special</Button>
          </div>
          <Divider />
          <div>
            <DisplayWorkDays workDays={this.state.specialWorkDays}
                             workDaysType="special"
                             handleDeleteWorkDay={this.handleDeleteWorkDay}
                             allowEmpty={true}/>
          </div>
          <CreateSpecialWorkDay open={this.state.createSpecial}
                                handleClose={() => this.setState({createSpecial: false})}
                                createRange={this.state.worker.turns_create_range}
                                onCreate={this.createSpecial}
                                workerId={this.state.worker.id} />
        </div>
      )
    } else {
      return (<div>Techenical issues</div>)
    }
  }
}

DisplaySpecials.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapDispatchToProps = dispatch => {
  return {
    onSaveWorker: (worker) => dispatch({
      type: "SAVE_WORKER",
      worker: worker
    })
  };
};

const mapStateToProps = state => {
  return {
    worker: state.worker
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(DisplaySpecials));