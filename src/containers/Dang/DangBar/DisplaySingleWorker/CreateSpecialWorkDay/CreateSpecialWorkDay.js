import React, {Component} from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import CloseIcon from '@material-ui/icons/Close';
import Divider from '@material-ui/core/Divider';
import Dialog from '@material-ui/core/Dialog';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Slide from '@material-ui/core/Slide';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import {authenticatedAxios} from '../../../../../utils/axiosInstances';
import DangTextField from '../../../../../components/DangFields/DangTextField';
import DangCreateDuty from '../../Workers/CreateWorker/DangCreateWorkDay/DangCreateDuty/DangCreateDuty';
import DangDisplayDuty from '../../Workers/CreateWorker/DangCreateWorkDay/DangDisplayDuty/DangDisplayDuty';

const Transition = props => {
  return <Slide direction="up" {...props} />;
}

const styles = theme => ({
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  flex: {
    flex: 1,
  },
  appBar: {
    position: 'relative',
  },
  button: {
    margin: theme.spacing.unit,
  },
  container: {
    padding: theme.spacing.unit,
  },
  errorText: {
    color: "red",
    opacity: "0.8",
  }
});

class CreateSpecialWorkDay extends Component {
  classes = this.props.classes
  state = {
    duties: [],
    specialDate: {value: moment().add(this.props.createRange, 'd').format('YYYY-MM-DD'), error: false},
    errors: [],
    isNotWorking: false,
  }
  onClose = () => {
    this.setState({
      workDays: [],
      specialDate: {value: moment().add(this.props.createRange, 'd').format('YYYY-MM-DD'), error: false},
      isNotWorking: false
    })
    this.props.handleClose();
  }
  handleChange = field => (value, error) => {
    let entity = {...this.state[field]};
    entity.value = value
    entity.error = error

    this.setState({[field]: entity});
  }
  onCreateSpecial = () => {
    authenticatedAxios.post('api/internal/workday/', {
      duties: this.isNotWorking ? [] : this.state.duties,
      date: this.state.specialDate.value,
      type: "special_work_day",
      worker: this.props.workerId
    })
      .then( (res) => {
        this.props.onCreate(res.data);
        this.onClose();
      })
      .catch( (err) => this.setState({errors: this.extractErrors(err.response.data)}))
  }
  addNewDuty = (turn_time, start_time, end_time) => {
    let currentDuties = [...this.state.duties];

    currentDuties.push({
      turn_time_minutes: turn_time,
      duty_start_time: start_time,
      duty_end_time: end_time
    });

    this.setState({duties: currentDuties});
  }
  getLastEndTime = () => {
    const duties = this.state.duties;
    if (!duties.length) {
      return null;
    }
    return duties[duties.length -1].duty_end_time;
  }
  onDeleteDuty = index => () => {
    let duties = [...this.state.duties];

    duties.splice(index, 1);
    this.setState({duties: duties});
  }
  extractErrors = errorsObject => {
    return Object.values(errorsObject).map((entityErrors => {
      return entityErrors.map( error => <li key={error} className={this.classes.errorText}>{error}</li>)
    }))
  }
  render() {
    return (
      <Dialog open={this.props.open}
              fullScreen
              TransitionComponent={Transition}
              onClose={this.onClose}>
        <AppBar className={this.classes.appBar}>
          <Toolbar>
            <IconButton color="inherit" onClick={this.props.handleClose} aria-label="Close">
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" className={this.classes.flex}>
              Create One-Time Work Day
            </Typography>
          </Toolbar>
        </AppBar>
        <div className={this.classes.container}>
          <h4>Pick a one-time day to change</h4>
          <small>
            Which date can you pick?
            <ul>
              <li>Future date</li>
              <li>Date without created turns</li>
              <li>Date which not already One-Time work day</li>
            </ul>
          </small>
          <Divider />
          <DangTextField value={this.state.specialDate.value}
                         type="date"
                         label="Date"
                         handleChange={this.handleChange('specialDate')}
                          />
          <h4>Work Day Schedule</h4>
          <Divider />
          <FormControl component="fieldset">
            <FormGroup>
              <FormControlLabel control={
                                  <Checkbox checked={this.state.isNotWorking}
                                            onChange={(event) => this.setState({isNotWorking: event.target.checked})}
                                            value="gilad" />
                                }
                                label="Day Off - Create no turns for this date"
            />
            </FormGroup>
          </FormControl>
          <Divider />
          {this.state.isNotWorking ? null :
            <div>
              <div>
                <DangCreateDuty addNewDuty={this.addNewDuty} lastEndTime={this.getLastEndTime()}/>
                <Divider />
              </div>
              <div>
                {this.state.duties.length ? 
                  <ul>
                    {this.state.duties.map((duty, index) => 
                      <li key={index}><DangDisplayDuty duty={duty} onDelete={this.onDeleteDuty(index)} /></li>)}
                  </ul> : <small className={this.classes.errorText}>Please add at least one duty</small> }
              </div>
              <Divider />
            </div> }
          <div>
            <ul>{this.state.errors}</ul>
            <Button className={this.classes.button}
                    variant="contained"
                    color="primary"
                    disabled={!this.state.duties.length > 0 && !this.state.isNotWorking}
                    onClick={this.onCreateSpecial}>Add WorkDay</Button>
            <Button className={this.classes.button}
                    variant="contained"
                    onClick={() => this.setState({duties: []})}
                    disabled={!this.state.duties.length > 0 && !this.state.isNotWorking}>Reset</Button>
            <Button className={this.classes.button}
                    variant="contained"
                    color="secondary"
                    onClick={this.onClose}>Cancel</Button>
          </div>
        </div>
      </Dialog>
    )
  }
}

CreateSpecialWorkDay.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CreateSpecialWorkDay);