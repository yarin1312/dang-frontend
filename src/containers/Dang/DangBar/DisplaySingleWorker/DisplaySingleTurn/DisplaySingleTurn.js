import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const styles = theme => ({
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
  button: {
    marginRight: theme.spacing.unit,
  },
  grow: {
    flexGrow: 1,
  },
});

const displaySingleTurn = props => {
  const { classes } = props;

  return (
    <ExpansionPanel expanded={props.isExpanded} onChange={props.handleChange}>
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <Typography className={classes.heading}>{props.turn.turn_time}</Typography>
        <Typography className={classes.secondaryHeading}>
          {props.turn.owner ? 
            <span style={{color: "red"}}>{props.turn.owner.full_name}</span> : 
            <span style={{color: "green"}}>Free Turn</span>}
        </Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        <div>
          <Button variant="outlined" color="primary" className={classes.button}>Order Turn</Button>
          <Button variant="outlined" color="secondary" className={classes.button}>Remove</Button>
        </div>
      </ExpansionPanelDetails>
    </ExpansionPanel>
  )
}

displaySingleTurn.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(displaySingleTurn);