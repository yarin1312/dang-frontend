import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import ArrowBack from '@material-ui/icons/ArrowBack';
import Divider from '@material-ui/core/Divider';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FilledInput from '@material-ui/core/FilledInput';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';

import {authenticatedAxios} from '../../../../../utils/axiosInstances';
import DangCreateWorkDay from '../../Workers/CreateWorker/DangCreateWorkDay/DangCreateWorkDay';
import DangTextField from '../../../../../components/DangFields/DangTextField';
import DisplayWorkDays from '../../Workers/CreateWorker/DisplayWorkDays/DisplayWorkDays';

const styles = theme => ({
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  footer: {
    marginTop: theme.spacing.unit * 2,
  },
  button: {
    margin: theme.spacing.unit,
  },
  grow: {
    flexGrow: 1,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 300,
  },
  marginTopOnly: {
    marginTop: theme.spacing.unit,
  }
});

class DisplayWorkerSettings extends Component {
  classes = this.props.classes
  state = {
    worker: null,
    workDays: [],
    worker_name: null,
    scheduleEdit: false,
    turns_creation_range: null
  }
  componentDidMount = () => {
    if (!this.props.worker) {
      const workerId = this.props.match.params.workerId;
      authenticatedAxios.get('api/internal/worker/' + workerId + '/')
        .then( (res) => {
          this.setState({
            worker: res.data,
            workDays: this.extractWeeklyWorkDays(res.data.workdays),
            worker_name: {value: res.data.worker_name, error: false},
            turns_creation_range: {value: res.data.turns_creation_range, error: false},
          });
          this.props.onSaveWorker(res.data);
        })
        .catch( (err) => console.log(err))
    } else {
      this.setState({
        worker: this.props.worker,
        workDays: this.extractWeeklyWorkDays(this.props.worker.workdays),
        worker_name: {
          value: this.props.worker.worker_name,
          error: false
        },
        turns_creation_range: {
          value: this.props.worker.turns_creation_range,
          error: false
        }
      })
    }
  }
  handleCreateWorkDay = (workDay) => {
    workDay.type = "weekly_work_day";
    workDay.worker = this.state.worker.id;

    authenticatedAxios.post('api/internal/workday/', workDay)
      .then( (res) => {
        const currentWorkDays = [...this.state.worker.workdays];
        currentWorkDays.push(res.data);
        const updatedWorker = this.state.worker;
        updatedWorker.workdays = currentWorkDays;

        this.setState({
          workDays: this.extractWeeklyWorkDays(currentWorkDays),
          worker: updatedWorker
        });
        this.props.onSaveWorker(updatedWorker);
      })
      .catch( (err) => console.log(err))

  }
  handleDeleteWorkDay = (workdayToDelete) => () => {
    authenticatedAxios.delete('api/internal/workday/' + workdayToDelete.id + '/')
      .then( (res) => {
        const workdays = this.state.worker.workdays.filter( workday => workday.id !== workdayToDelete.id);
        const updatedWorker = this.state.worker;
        updatedWorker.workdays = workdays;

        this.setState({
          workDays: this.extractWeeklyWorkDays(workdays),
          worker: updatedWorker
        });
        this.props.onSaveWorker(updatedWorker);
      })
      .catch( (err) => console.log(err))
  }
  handleChange = field => (value, error) => {
    console.log(value)
    let entity = {...this.state[field]};
    entity.value = value
    entity.error = error

    this.setState({[field]: entity});
  }
  handleReset = () => {
    this.setState({
      worker_name: {
        value: this.state.worker.worker_name,
        error: false
      }
    })
  }
  onCacnel = () => {
    this.handleReset();
    this.props.history.push('/worker/' + this.props.match.params.workerId);
  }
  onSaveWorker = () => {
    let dataToUpdate = {}
    if (this.state.worker_name.value !== this.state.worker.worker_name) {
      dataToUpdate.worker_name = this.state.worker_name.value;
    }
  }
  extractWeeklyWorkDays = (allWorkDays) => {
    return allWorkDays.filter( workDay => workDay.type === 'weekly_work_day');
  }
  saveNewName = () => {
    authenticatedAxios.patch('api/internal/worker/' + this.state.worker.id + '/',
                             {worker_name: this.state.worker_name.value})
      .then( (res) => {
        this.setState({
          worker: res.data,
          worker_name: {
            value: res.data.worker_name,
            error: false
          }
        })
        this.props.onSaveWorker(res.data);
      })
      .catch( (err) => console.log(err))
  }
  onUpdateWorker = prop => () => {
    authenticatedAxios.patch('api/internal/worker/' + this.state.worker.id + '/',
                             {[prop]: this.state[prop].value})
      .then( (res) => {
        this.setState({
          worker: res.data,
          [prop]: {
            value: res.data[prop],
            error: false
          }
        })
        this.props.onSaveWorker(res.data);
      })
      .catch( (err) => console.log(err))
  }
  render() {
    if (this.state.worker) {
      const daysOfWeek = this.state.workDays.map((workday) => workday.day_of_week.toString());
      return (
        <div>
          <h2 style={{paddingLeft: "10px"}}>
            <IconButton onClick={() => this.props.history.push('/worker/' + this.state.worker.id)}>
              <ArrowBack />
            </IconButton>
            Worker Settings
          </h2>
          <Divider />
          <div>
            <h4>Worker Details</h4>
            <div>
              <DangTextField required
                             type="text"
                             value={this.state.worker_name.value}
                             handleChange={this.handleChange('worker_name')}
                             label="Worker Name"
                             className={this.classes.textField} />
            </div>
            <div>
              <Button className={this.classes.button}
                      variant="outlined"
                      color="primary"
                      disabled={!this.state.worker_name.value || this.state.worker_name.value === this.state.worker.worker_name}
                      onClick={this.onUpdateWorker('worker_name')}>Save</Button>
              <Button className={this.classes.button}
                      variant="outlined"
                      disabled={this.state.worker_name.value === this.state.worker.worker_name}
                      onClick={this.handleReset}>RESET</Button>
            </div>
            <div className={this.classes.marginTopOnly}>
              <FormControl variant="filled" className={this.classes.textField}>
                <InputLabel htmlFor="turns-creation-range">Turn Creation Range</InputLabel>
                <Select value={this.state.turns_creation_range.value}
                        onChange={(event) => this.handleChange('turns_creation_range')(event.target.value, false)}
                        input={<FilledInput name="turns_creation_range" id="turns-creation-range" />}>
                  {(new Array(30)).fill().map((val, index) =>
                    <MenuItem key={index} value={index + 1}>{index + 1}</MenuItem>
                  )}
                </Select>
                <FormHelperText>The range of days to create new turns</FormHelperText>
              </FormControl>
            </div>
            <div>
              <Button className={this.classes.button}
                      variant="outlined"
                      color="primary"
                      disabled={this.state.turns_creation_range.value === this.state.worker.turns_creation_range}
                      onClick={this.onUpdateWorker('turns_creation_range')}>Save</Button>
              <Button className={this.classes.button}
                      variant="outlined"
                      disabled={this.state.turns_creation_range.value === this.state.worker.turns_creation_range}
                      onClick={() => this.setState({turns_creation_range: this.state.worker.turns_creation_range})}>
                RESET
              </Button>
            </div>
          </div>
          <div>
            <h4>Weekly Worker Schedule</h4>
            <div>
              <DisplayWorkDays workDays={this.state.workDays}
                               handleDeleteWorkDay={this.handleDeleteWorkDay}
                               readOnly={!this.state.scheduleEdit}
                               allowEmpty={true} />
            </div>
            {this.state.scheduleEdit ? 
              <div>
                <ExpansionPanel>
                  <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={this.classes.heading}>Add New Weekly Work Day:</Typography>
                  </ExpansionPanelSummary>
                  <ExpansionPanelDetails>
                    <DangCreateWorkDay onCreate={this.handleCreateWorkDay} daysOfWeek={daysOfWeek}/>
                  </ExpansionPanelDetails>
                </ExpansionPanel>
                <Button className={this.classes.button}
                        color="secondary"
                        variant="outlined"
                        onClick={() => this.setState({scheduleEdit: false})}>Off Edit Mode</Button>
              </div> : <Button className={this.classes.button}
                               variant="outlined"
                               onClick={() => this.setState({scheduleEdit: true})}
                               color="primary">Edit Schedule</Button>}
          </div>
        </div>
      ) 
    }
    return (
      <h1>Error occured</h1>
    )
  }
}

const mapStateToProps = state => {
  return {
    worker: state.worker
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onSaveWorker: (worker) => dispatch({
      type: "SAVE_WORKER",
      worker: worker
    })
  };
};

DisplayWorkerSettings.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(DisplayWorkerSettings));