import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Button from "@material-ui/core/Button";
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import DangTextField from '../../../../components/DangFields/DangTextField';
import {emailValidation,
  passwordValidation,
  phoneNumberValidation} from '../../../../utils/validators';
import { authenticatedAxios } from '../../../../utils/axiosInstances';
import EditableSelectField from '../../../../components/EditableFields/EditableSelectField';
import EditableTextField from '../../../../components/EditableFields/EditableTextField';

const styles = theme => ({
  slugContainer: {
    backgroundColor: "lightgray",
    padding: theme.spacing.unit * 2,
    fontFamily: "Courier New",
    display: "inline-block",
    marginTop: theme.spacing.unit
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 300,
    backgroundColor: "white",
  },
  button: {
    margin: theme.spacing.unit
  },
  topSpacing: {
    marginTop: theme.spacing.unit,
  }
});

const timezones = [];

class Business extends Component {
  classes = this.props.classes
  onUpdateBusiness = fieldName => val => {
    authenticatedAxios.patch('api/internal/business/' + this.props.user.business.id + '/',
                             {[fieldName]: val || null})
      .then( res => {
        const user = {...this.props.user}
        user.business = res.data
        this.props.onUpdateUser(user)
      })
      .catch( (err) => console.log(err))
  }
  onUpdateUser = fieldName => val => {
    authenticatedAxios.patch('api/internal/user/' + this.props.user.id + '/',
                             {[fieldName]: val || null})
      .then( res => this.props.onUpdateUser(res.data))
      .catch( err => console.log(err))
  }
  render() {
    const business = this.props.user.business;
    const user = this.props.user;
    const workersColor = business.worker_limitation > business.number_of_workers ? "green" : "red";
    const clientsColor = business.clients_limitation > business.number_of_clients ? "green" : "red";
    return (
      <div>
        <h2 style={{paddingLeft: "10px"}}>{business.business_name}</h2>
        <Divider />
        <div style={{textAlign: "center"}}>
          <div className={this.classes.slugContainer}>
            <div style={{fontWeight: "Bold"}}>Company Code:</div>
            {business.slug}
          </div>
          <div className={this.classes.topSpacing}>
            <Paper>
              <Table>
                <TableBody>
                  <TableRow>
                    <TableCell component="th" scope="row" style={{fontWeight: "bold"}}>Workers</TableCell>
                    <TableCell style={{color: workersColor}}>
                      {business.number_of_workers}/{business.worker_limitation}
                    </TableCell>
                    <TableCell><a href="#">Increase</a></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell component="th" scope="row" style={{fontWeight: "bold"}}>Clients</TableCell>
                    <TableCell style={{color: clientsColor}}>
                      {business.number_of_clients}/{business.clients_limitation}
                    </TableCell>
                    <TableCell>Increase</TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </Paper>
          </div>
        </div>
        <h2>Details</h2>
        <Divider />
        <div style={{textAlign: "center"}}>
          <form>
            <div>
              <EditableTextField required
                                 label="Business Name"
                                 value={business.business_name}
                                 onSave={this.onUpdateBusiness('business_name')} />
            </div>
            <div>
              <EditableTextField label="Business Host"
                                 value={business.business_host}
                                 helperText="Enter your business's host to allow api request"
                                 onSave={this.onUpdateBusiness('business_host')} />
            </div>
            <div>
              <EditableTextField type="tel"
                                 value={user.contact_phone_number}
                                 validation={phoneNumberValidation}
                                 label="Contact phone number"
                                 onSave={this.onUpdateUser('contact_phone_number')} />
            </div>
            <div>
              <EditableSelectField elements={timezones}
                                   value={business.business_timezone}
                                   label="Business Timezone"
                                   onSave={this.onUpdateBusiness('business_timezone')} />
            </div>
          </form>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.user,
    token: state.token
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onUpdateUser: (user) => dispatch({
      type: "USER_UPDATE",
      user: user
    })
  };
};

Business.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Business));