import React, { Component } from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';

import {authenticatedAxios} from '../../../../utils/axiosInstances';
import CreateWorker from './CreateWorker/CreateWorker';
import DisplayWorkerCard from './DisplayWorkerCard/DisplayWorkerCard';

const style = theme => ({
  header: {
    display: "flex"
  },
  grow: {
    flexGrow: 1,
  }
});

class Workers extends Component {
  classes = this.props.classes
  state = {
    workers: [],
    createWorker: false
  }
  componentDidMount = () => {
    authenticatedAxios.get("api/internal/worker/")
      .then( (res) => {
        this.setState({workers: res.data.results})
      })
      .catch( (err) => console.log(err))
  }
  addNewWorker = (workerData) => {
    const data = {
      business: this.props.user.business.id,
      ...workerData
    }
    authenticatedAxios.post('api/internal/worker/', data)
      .then((res) => {
        let workers = [...this.state.workers];
        workers.push(res.data);

        this.setState({workers: workers, createWorker: false});
      })
      .catch((err) => console.log(err))
  }
  deleteWorker = (worker) => () => {
    authenticatedAxios.delete('api/internal/worker/' + worker.id + '/')
      .then( (res) => {
        const workers = [...this.state.workers]
        const newWorkers = workers.filter( w => w.id !== worker.id)
        this.setState({workers: newWorkers})
      })
      .catch( err => console.log(err))
  }
  render() {
    const worker_full = this.state.workers.length === this.props.user.business.worker_limitation;
    return (
      <div>
        <div className={this.classes.header}>
          <h2 className={this.classes.grow} style={{paddingLeft: "10px"}}>Workers</h2>
          <h5 style={{color: worker_full ? "red" : "green"}}>
            Worker Limitation: {this.props.user.business.worker_limitation}
          </h5>
        </div>
        <Divider />
        <div>{this.state.workers.length > 0 ? null : <p>No workers added</p>}</div>
        <div>
          {this.state.workers.map( (worker, index) =>
             <DisplayWorkerCard key={index}
                                workerName={worker.worker_name}
                                workerId={worker.id}
                                history={this.props.history}
                                deleteWorker={this.deleteWorker(worker)}/>)}
        </div>
        <div>
          <Button variant="outlined"
                  color="primary"
                  onClick={() => this.setState({createWorker: true})}
                  disabled={worker_full}>Add worker</Button>
        </div>
        <CreateWorker open={this.state.createWorker}
                      handleClose={() => this.setState({createWorker: false})}
                      onCreateWorker={this.addNewWorker} />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.user,
    token: state.token
  };
};

Workers.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default connect(mapStateToProps)(withStyles(style)(Workers));