import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';

import DisplayWorkDay from './DisplayWorkDay/DisplayWorkDay';

const styles = theme => ({
  errorText: {
    color: "red",
    opacity: "0.8"
  },
  marginBottomCls: {
    marginBottom: theme.spacing.unit * 2,
  },
});

const displayWorkDays = props => {
  const classes = props.classes;
  return (
    <div>
      <List>
        {props.workDays.map((workday, index) => 
          <DisplayWorkDay key={index}
                          type={props.workDaysType}
                          workday={workday}
                          onDelete={props.handleDeleteWorkDay(workday)}
                          readOnly={props.readOnly}/>)}
      </List>
      {props.workDays.length || props.allowEmpty ? null : 
        <div className={classes.marginBottomCls}>
          <small className={classes.errorText}>Add at least one Weekly Work Day</small>
        </div>}
    </div>
  )
}

displayWorkDays.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(displayWorkDays);