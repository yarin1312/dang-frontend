import React from 'react';
import moment from 'moment';

import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';

import {dayOfWeekToString} from '../../../../../../../utils/datetimeUtils';

const displayWorkDay = (props) => {
  let dutiesString = ""
  const duties = props.workday.duties;
  if (duties.length) {
    for (var i in props.workday.duties) {
      const duty = props.workday.duties[i];
      dutiesString = dutiesString + duty.duty_start_time + " - " + duty.duty_end_time + " | " + duty.turn_time_minutes + "Min ";
    }
  } else {
    dutiesString = "Day off - NO TURNS";
  }
  const primary = props.type === 'special' ? moment(props.workday.date, "YYYY-MM-DD").format("DD/MM/YYYY") : dayOfWeekToString[props.workday.day_of_week];
  return (
    <ListItem divider={true}>
      {props.readOnly ? null : <IconButton aria-label="Delete" onClick={props.onDelete}>
        <DeleteIcon />
      </IconButton>}
      <ListItemText primary={primary} secondary={dutiesString}/>
      <ListItemSecondaryAction>
      </ListItemSecondaryAction>
    </ListItem>
  );
}

export default displayWorkDay;