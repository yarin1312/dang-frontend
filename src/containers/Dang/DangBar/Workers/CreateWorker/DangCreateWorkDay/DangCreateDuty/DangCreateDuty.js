import React, {Component} from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';
import FilledInput from '@material-ui/core/FilledInput';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';

import DangTextField from '../../../../../../../components/DangFields/DangTextField';
import {digitsOnlyNoZero} from '../../../../../../../utils/validators';
import {calculateTurns, calculateTurnsFromTime} from '../../../../../../../utils/datetimeUtils';

const style = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 130,
  },
  formContainer: {
    display: 'inline-block',
  },
  selectField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 130,
  },
  addDutyButton: {
    marginTop: theme.spacing.unit * 3,
  }
});

class DangCreateDuty extends Component {
  classes = this.props.classes
  state = {
    duty_start_time: "",
    duty_end_time: "",
    turn_time_minutes: {value: "", error: false}
  }
  handleChange = field => (value, error) => {
    let entity = {...this.state[field]};
    entity.value = value
    entity.error = error

    this.setState({[field]: entity});
  }
  onChangeSelect = event => {
    this.setState({[event.target.name]: event.target.value});
  }
  initDuty = () => {
    this.setState({
      duty_start_time: "",
      duty_end_time: "",
      turn_time_minutes: {value: "", error: false}
    })
  }
  addDuty = (form) => {
    form.preventDefault();
    
    this.props.addNewDuty(this.state.turn_time_minutes.value,
                          this.state.duty_start_time,
                          this.state.duty_end_time);
    this.initDuty();
  }
  render() {
    let start_times = [];
    let end_times = [];
    const turn_time = parseInt(this.state.turn_time_minutes.value)
    if (turn_time) {
      start_times = this.props.lastEndTime ?
        calculateTurnsFromTime(turn_time, this.props.lastEndTime) : calculateTurns(turn_time);
      if (this.state.duty_start_time) {
        end_times = calculateTurnsFromTime(turn_time,
                                           this.state.duty_start_time,
                                           true);
      }
    }
    return (
      <form className={this.classes.formContainer} onSubmit={this.addDuty}>
        <DangTextField className={this.classes.textField}
                       label="Eact Turn Time"
                       helperText="*Turn time in minutes"
                       value={this.state.turn_time_minutes.value}
                       handleChange={this.handleChange('turn_time_minutes')}
                       validation={digitsOnlyNoZero}
                       intputOtherProps={{maxLength: "2"}}
                       otherProps={{variant: "filled"}}
                       />
          <FormControl variant="filled"
                       margin="normal"
                       className={this.classes.selectField}
                       disabled={turn_time ? false : true}>
            <InputLabel
              ref={ref => {
                this.InputLabelRef = ref;
              }}
              htmlFor="outlined-start-time-native-simple">
              Start Time
            </InputLabel>
            <Select native
                    value={this.state.duty_start_time}
                    onChange={this.onChangeSelect}
                    input={
                      <FilledInput
                        name="duty_start_time"
                        id="outlined-start-time-native-simple"
                      />
                    }>
              <option value="" />
              {start_times.map( (time, index) => <option key={index} value={time}>{time}</option>)}
            </Select>
          </FormControl>
          <FormControl variant="filled"
                       margin="normal"
                       className={this.classes.selectField}
                       disabled={this.state.duty_start_time !== "" ? false : true}>
            <InputLabel
              ref={ref => {
                this.InputLabelRef = ref;
              }}
              htmlFor="outlined-end-time-native-simple">
              End Time
            </InputLabel>
            <Select native
                    value={this.state.duty_end_time}
                    onChange={this.onChangeSelect}
                    input={
                      <FilledInput
                        name="duty_end_time"
                        id="outlined-end-time-native-simple"
                      />
                    }>
              <option value="" />
              {end_times.map( (time, index) => <option key={index} value={time}>{time}</option>)}
            </Select>
          </FormControl>
          <Button size="small"
                  className={this.classes.addDutyButton}
                  variant="outlined"
                  color="primary"
                  type="submit"
                  disabled={this.state.duty_start_time && this.state.duty_end_time && this.state.turn_time_minutes.value ? false : true}>
            <AddIcon />add duty
          </Button>
      </form>
    );
  }
}

DangCreateDuty.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(style)(DangCreateDuty);