import React, {Component} from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import FilledInput from '@material-ui/core/FilledInput';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';

import DangCreateDuty from './DangCreateDuty/DangCreateDuty';
import DangDispayDuty from './DangDisplayDuty/DangDisplayDuty';
import {dayOfWeekToString} from '../../../../../../utils/datetimeUtils';
import { Button } from '@material-ui/core';

const styles = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  container: {
    marginTop: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit * 2,
  },
  errorText: {
    color: "red",
    opacity: "0.8"
  }
});

class DangCreateWorkDay extends Component {
  classes = this.props.classes
  state = {
    day_of_week: "",
    duties: []
  }
  // handleChange = field => (value, error) => {
  //   let entity = {...this.state[field]};
  //   entity.value = value
  //   entity.error = error

  //   this.setState({[field]: entity});
  // }
  handleChange = event => {
    this.setState({[event.target.name]: event.target.value});
  }
  handleCreateDuty = (turn_time, start_time, end_time) => {
    let currentDuties = [...this.state.duties];

    currentDuties.push({
      turn_time_minutes: turn_time,
      duty_start_time: start_time,
      duty_end_time: end_time
    });

    this.setState({duties: currentDuties});
  }
  getLastEndTime = () => {
    const duties = this.state.duties;
    if (!duties.length) {
      return null;
    }
    return duties[duties.length -1].duty_end_time;
  }
  onDeleteDuty = index => () => {
    let duties = [...this.state.duties];

    duties.splice(index, 1);
    this.setState({duties: duties});
  }
  onCreateWorkDay = () => {
    this.props.onCreate({
      day_of_week: this.state.day_of_week,
      duties: [...this.state.duties]
    });
    this.setState({day_of_week: "", duties: []});
  }
  render () {
    return (
      <div>
        <div>
          <FormControl variant="filled" margin="normal" className={this.classes.textField}>
            <InputLabel
              ref={ref => {
                this.InputLabelRef = ref;
              }}
              htmlFor="outlined-day-of-week-native-simple">
              Day Of Week*
            </InputLabel>
            <Select native
                    value={this.state.day_of_week}
                    onChange={this.handleChange}
                    input={
                      <FilledInput
                        name="day_of_week"
                        id="outlined-day-of-week-native-simple"
                      />
                    }>
              <option value="" />
              <option value={6} disabled={this.props.daysOfWeek.indexOf('6') !== -1}>Sunday</option>
              <option value={0} disabled={this.props.daysOfWeek.indexOf('0') !== -1}>Monday</option>
              <option value={1} disabled={this.props.daysOfWeek.indexOf('1') !== -1}>Tuesday</option>
              <option value={2} disabled={this.props.daysOfWeek.indexOf('2') !== -1}>Wednesday</option>
              <option value={3} disabled={this.props.daysOfWeek.indexOf('3') !== -1}>Thursday</option>
              <option value={4} disabled={this.props.daysOfWeek.indexOf('4') !== -1}>Friday</option>
              <option value={5} disabled={this.props.daysOfWeek.indexOf('5') !== -1}>Saturday</option>
            </Select>
          </FormControl>
        </div>
        <div>
          {this.state.day_of_week !== "" ? <DangCreateDuty addNewDuty={this.handleCreateDuty} lastEndTime={this.getLastEndTime()}/> : null}
        </div>
        <div>
          <span style={{fontWeight: "bold", textDecoration: "underline"}}>Preview before adding:</span>
        </div>
        {this.state.day_of_week ? 
          (<div>
            <div className={this.classes.container}>
              <span style={{fontWeight: "bold"}}>{dayOfWeekToString[this.state.day_of_week]}: </span>
            </div>
            {this.state.duties.length ? 
              <ul>
                {this.state.duties.map((duty, index) => 
                  <li key={index}><DangDispayDuty duty={duty} onDelete={this.onDeleteDuty(index)} /></li>)}
            </ul> : <small className={this.classes.errorText}>Please add at least one duty</small> }
            {this.state.duties.length ? 
              <Button variant="extendedFab"
                      color="primary"
                      aria-label="Add Work Day"
                      onClick={this.onCreateWorkDay}>Add Work Day</Button> : null}
          </div>) : <span>Please fill details</span>}
      </div>
    )
  }
}

DangCreateWorkDay.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DangCreateWorkDay);