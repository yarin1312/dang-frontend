import React from 'react'

import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';

const displayDuty = (props) => {
  return (
    <span>
      {props.duty.duty_start_time} - {props.duty.duty_end_time} | {props.duty.turn_time_minutes + "Min"}
      <IconButton aria-label="Delete" onClick={props.onDelete}>
        <DeleteIcon fontSize="small" />
      </IconButton>
    </span>
  )
}

export default displayDuty