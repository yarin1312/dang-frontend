import React, {Component} from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import FilledInput from '@material-ui/core/FilledInput';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';

import CreateDuty from './CreateDuty/CreateDuty';

const styles = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
});

class CreateWorkDay extends Component {
  classes = this.props.classes;
  state = {
    day_of_week: {value: "", error: false},
    duties: [],
  }

  handleChange = field => (value, error) => {
    let entity = {...this.state[field]};
    entity.value = value
    entity.error = error

    this.setState({[field]: entity});
  }
  render() {
    return (
      <Dialog open={this.props.open} onClose={this.props.handleClose}>
        <DialogTitle>Create Weekly Work Day</DialogTitle>
        <DialogContent>
          <FormControl className={this.classes.textField}>
            <InputLabel
              ref={ref => {
                this.InputLabelRef = ref;
              }}
              htmlFor="outlined-day-of-week-native-simple">
              Day Of Week
            </InputLabel>
            <Select native
                    value={this.state.day_of_week.value}
                    input={
                      <FilledInput
                        name="day_of_week"
                        id="outlined-day-of-week-native-simple"
                      />
                    }>
              <option value="" />
              <option value={0}>Sunday</option>
              <option value={1}>Monday</option>
              <option value={2}>Tuesday</option>
              <option value={3}>Wednesday</option>
              <option value={4}>Thursday</option>
              <option value={5}>Friday</option>
              <option value={6}>Saturday</option>
            </Select>
          </FormControl>
          <CreateDuty />
        </DialogContent>
      </Dialog>
    )
  }
}

CreateWorkDay.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CreateWorkDay);