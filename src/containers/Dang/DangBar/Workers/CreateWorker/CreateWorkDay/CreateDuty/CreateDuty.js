import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {withStyles} from '@material-ui/core/styles';
import FilledInput from '@material-ui/core/FilledInput';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';

import DangTextField from '../../../../../../../components/DangFields/DangTextField';
import {phoneNumberValidation} from '../../../../../../../utils/validators';
import {calculateTurns, calculateTurnsFromTime} from '../../../../../../../utils/datetimeUtils';


const style = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 130,
  },
  formContainer: {
    display: 'inline-block',
  },
  selectField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 130,
  }
});

class CreateDuty extends Component {
  classes = this.props.classes
  state = {
    duty_start_time: "",
    duty_end_time: "",
    turn_time_minutes: {value: "", error: false}
  }
  handleChange = field => (value, error) => {
    let entity = {...this.state[field]};
    entity.value = value
    entity.error = error

    this.setState({[field]: entity});
  }
  render() {
    let start_times = [];
    let end_times = [];
    if (this.state.turn_time_minutes.value) {
      start_times = calculateTurns(parseInt(this.state.turn_time_minutes.value));
      if (this.state.duty_start_time) {
        end_times = calculateTurnsFromTime(this.state.turn_time_minutes.value,
                                           this.state.duty_start_time);
      }
    }

    return (
      <form className={this.classes.formContainer}>
        <DangTextField className={this.classes.textField}
                       label="Eact Turn Time"
                       helperText="*Turn time in minutes"
                       value={this.state.turn_time_minutes.value}
                       handleChange={this.handleChange('turn_time_minutes')}
                       validation={phoneNumberValidation}
                       intputOtherProps={{maxLength: "2"}}
                       otherProps={{variant: "filled"}}
                       />
          <FormControl variant="filled" margin="normal" className={this.classes.selectField}>
            <InputLabel
              ref={ref => {
                this.InputLabelRef = ref;
              }}
              htmlFor="outlined-start-time-native-simple">
              Start Time
            </InputLabel>
            <Select native
                    value={this.state.duty_start_time}
                    input={
                      <FilledInput
                        name="duty_start_time"
                        id="outlined-start-time-native-simple"
                      />
                    }>
              <option value="" />
              {start_times.map( (time, index) => <option key={index} value={time}>{time}</option>)}
            </Select>
          </FormControl>
          <FormControl variant="filled" margin="normal" className={this.classes.selectField}>
            <InputLabel
              ref={ref => {
                this.InputLabelRef = ref;
              }}
              htmlFor="outlined-end-time-native-simple">
              End Time
            </InputLabel>
            <Select native
                    value={this.state.duty_start_time}
                    input={
                      <FilledInput
                        name="duty_end_time"
                        id="outlined-end-time-native-simple"
                      />
                    }>
              <option value="" />
              {end_times.map( (time, index) => <option key={index} value={time}>{time}</option>)}
            </Select>
          </FormControl>
      </form>
    )
  }
}

CreateDuty.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(style)(CreateDuty);