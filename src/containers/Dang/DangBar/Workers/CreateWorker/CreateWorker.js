import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import CloseIcon from '@material-ui/icons/Close';
import Dialog from '@material-ui/core/Dialog';
import Divider from '@material-ui/core/Divider';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Slide from '@material-ui/core/Slide';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FilledInput from '@material-ui/core/FilledInput';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import InputLabel from '@material-ui/core/InputLabel';

import DangCreateWorkDay from './DangCreateWorkDay/DangCreateWorkDay';
import DisplayWorkDays from './DisplayWorkDays/DisplayWorkDays';
import DangTextField from '../../../../../components/DangFields/DangTextField';

const styles = theme => ({
  appBar: {
    position: 'relative',
  },
  flex: {
    flex: 1,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 300,
  },
  addWorkDay: {
    marginTop: 3 * theme.spacing.unit,
  },
  addWorkerButton: {
    marginTop: theme.spacing.unit * 2,
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  block: {
    padding: theme.spacing.unit * 2
  }
});

const Transition = props => {
  return <Slide direction="up" {...props} />;
}

class CreateWorker extends Component {
  classes = this.props.classes
  state = {
    worker_name: {value: "", error: false},
    turns_creation_range: {value: 14, error: false},
    workDays: [],
    createNewWorkDay: false
  }
  handleChange = field => (value, error) => {
    let entity = {...this.state[field]};
    entity.value = value
    entity.error = error

    this.setState({[field]: entity});
  }
  handleCreateWorkDay = (workDay) => {
    let currentWorkDays = [...this.state.workDays];
    currentWorkDays.push(workDay);

    this.setState({workDays: currentWorkDays});
  }
  handleDeleteWorkDay = (workdayToRemove) => () => {
    const workdays = this.state.workDays.filter(workday => workday.day_of_week !== workdayToRemove.day_of_week);
    this.setState({workDays: workdays});
  }
  handleAddWorker = () => {
    this.props.onCreateWorker({
      workdays: this.state.workDays,
      worker_name: this.state.worker_name.value,
      turns_creation_range: this.state.turns_creation_range.value,
    })
    this.setState({
      worker_name: {value: "", error: false},
      turns_creation_range: {value: 14, error: false},
      workDays: [],
    });
  }
  render() {
    const daysOfWeek = this.state.workDays.map((workday) => workday.day_of_week);
    const addNewWorkerAllow = this.state.worker_name.value.length > 0 && !this.state.worker_name.error && this.state.workDays.length > 0;
    return (
      <Dialog fullScreen 
              open={this.props.open}
              TransitionComponent={Transition}
              onClose={this.props.handleClose}>
        <AppBar className={this.classes.appBar}>
          <Toolbar>
            <IconButton color="inherit" onClick={this.props.handleClose} aria-label="Close">
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" className={this.classes.flex}>
              Create new worker
            </Typography>
          </Toolbar>
        </AppBar>
        <div className={this.classes.block}>
          <h3>Worker Details</h3>
          <Divider />
          <div>
            <DangTextField required
                          type="text"
                          value={this.state.worker_name.value}
                          handleChange={this.handleChange('worker_name')}
                          label="Worker Name"
                          className={this.classes.textField}
                          />
          </div>
          <div>
            <FormControl variant="filled" className={this.classes.textField}>
              <InputLabel htmlFor="turns-creation-range">Turn Creation Range</InputLabel>
              <Select value={this.state.turns_creation_range.value}
                      onChange={(event) => this.handleChange('turns_creation_range')(event.target.value, false)}
                      input={<FilledInput name="turns_creation_range" id="turns-creation-range" />}>
                {(new Array(30)).fill().map((val, index) =>
                  <MenuItem key={index} value={index + 1}>{index + 1}</MenuItem>
                )}
              </Select>
              <FormHelperText>The range of days to create new turns</FormHelperText>
            </FormControl>
          </div>
          {/* <Button color="primary"
                  variant="contained"
                  className={this.classes.addWorkDay}
                  onClick={() => this.setState({createNewWorkDay: true})}>+ Add weekly work day</Button> */}
        </div>
        <div className={this.classes.block}>
          <h3>Worker Schedule</h3>
          <Divider />
          <div>
            <DisplayWorkDays workDays={this.state.workDays}
                             handleDeleteWorkDay={this.handleDeleteWorkDay}/>
            <Divider />
          </div>
          <div>
            <ExpansionPanel>
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                <Typography className={this.classes.heading}>Add New Weekly Work Day:</Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <DangCreateWorkDay onCreate={this.handleCreateWorkDay} daysOfWeek={daysOfWeek}/>
              </ExpansionPanelDetails>
            </ExpansionPanel>
            {/* <span style={{fontWeight: "bold", textDecoration: "underline"}}>Add Weekly Work Day:</span> */}
          </div>
          {/* <div>
            <DangCreateWorkDay onCreate={this.handleCreateWorkDay} daysOfWeek={daysOfWeek}/>
            <Divider />
          </div> */}
          <Button variant="contained"
                  color="primary"
                  className={this.classes.addWorkerButton}
                  disabled={!addNewWorkerAllow}
                  onClick={this.handleAddWorker}>
            Add New Worker
          </Button>
        </div>
      </Dialog>
    )
  }
}

CreateWorker.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CreateWorker);