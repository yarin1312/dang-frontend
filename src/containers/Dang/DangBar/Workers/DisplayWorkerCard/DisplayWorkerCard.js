import React, {Component} from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';

import ConfirmationDialog from '../../../../../components/ConfirmationDialog/ConfirmationDialog';

const styles = theme => ({
  card: {
    width: "300px",
    margin: theme.spacing.unit * 2,
    display: "inline-block",
  },
});

class DisplayWorkerCard extends Component {
  classes = this.props.classes
  state = {
    delete: false
  }
  onDelete = () => {
    this.props.deleteWorker()
    this.setState({delete: false})
  }
  render () {
    return (
      <div className={this.classes.card}>
        <Card>
          <CardContent>
            <h2>{this.props.workerName}</h2>
          </CardContent>
          <CardActions>
            <Button variant="outlined"
                    size="small"
                    color="primary"
                    onClick={() => this.props.history.push("/worker/" + this.props.workerId)}>Schedule</Button>
            <Button variant="outlined"
                    size="small"
                    onClick={() => this.props.history.push("/worker/" + this.props.workerId + "/settings")}>Settings</Button>
            <Button variant="outlined"
                    size="small"
                    onClick={() => this.setState({delete: true})}
                    color="secondary">Remove</Button>
          </CardActions>
        </Card>
        <ConfirmationDialog open={this.state.delete}
                            title="Delete Worker"
                            onAccept={this.onDelete}
                            onCancel={() => this.setState({delete: false})} />
      </div>
    )
  }
}

DisplayWorkerCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DisplayWorkerCard);