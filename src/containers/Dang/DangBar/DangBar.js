import React, {Component} from 'react'
import { Route, Switch, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';

import Business from './Business/Business';
import Clients from './Clients/Clients';
import DangDrawer from './DangDrawer/DangDrawer';
import Login from './Login/Login';
import Register from './Register/Register';
import Workers from './Workers/Workers';
import DisplaySingleWorker from './DisplaySingleWorker/DisplaySingleWorker';
import DisplayWorkerSettings from './DisplaySingleWorker/DisplayWorkerSettings/DisplayWorkerSettings';
import DisplaySpecials from './DisplaySingleWorker/DisplaySpecialWorkDays/DisplaySpecialWorkDays';

import {authenticatedAxios} from '../../../utils/axiosInstances';

const drawerWidth = 240;

const styles = theme => ({
  root: {
    flexGrow: 1,
    height: "100vh",
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
  },
  navIconHide: {
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawerPaper: {
    position: 'relative',
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 2,
    minWidth: 0, // So the Typography noWrap works
    overflowY: "scroll"
  },
  toolbar: theme.mixins.toolbar,
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: 20,
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
});

class DangBar extends Component{
  classes = this.props.classes;
  state = {
    menuOpen: false
  }
  componentDidMount = () => {
    const userId = localStorage.getItem('user_id')
    if (userId) {
      authenticatedAxios.get('api/internal/user/' + userId + '/')
        .then( res => this.props.onUserUpdate(res.data))
        .catch( err => console.log(err))
    }
  }

  handleLogout = () => {
    localStorage.setItem("user_id", null)
    localStorage.setItem("token", null)

    this.props.onUserLogout()
    window.location = window.location.origin
  }

  render () {
    const routesForUsers = [
      
    ]
    const drawerContent = (
      <div>
        <div className={this.classes.toolbar} />
        <DangDrawer user={this.props.user} history={this.props.history} />
      </div>
    )
    const rightPanel = this.props.user ? (
        <Button color="inherit" onClick={() => this.handleLogout()}>Logout</Button>
      ) : (<Button color="inherit" onClick={() => this.props.history.push("/login/")}>Login</Button>)
    
    return (
      <div className={this.classes.root}>
        <AppBar position="fixed" className={this.classes.appBar}>
          <Toolbar>
            <IconButton color="inherit"
                        aria-label="Open drawer"
                        className={this.classes.menuButton}
                        onClick={() => this.setState({menuOpen: !this.state.menuOpen})}>
              <MenuIcon />
            </IconButton>
            <span className={this.classes.grow}>
              Dang
            </span>
            {rightPanel}
          </Toolbar>
        </AppBar>
        <Hidden smUp implementation="css">
          <Drawer
            container={this.props.container}
            variant="temporary"
            anchor='left'
            open={this.state.menuOpen}
            onClose={() => this.setState({menuOpen: !this.state.menuOpen})}
            classes={{
              paper: this.classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            <div tabIndex={0}
                 role="button"
                 onClick={() => this.setState({menuOpen: false})}
                 onKeyDown={() => this.setState({menuOpen: false})}>
              {drawerContent}
            </div>
          </Drawer>
        </Hidden>
        <Hidden smDown>
          <Drawer
            variant="permanent"
            classes={{
              paper: this.classes.drawerPaper,
            }}
          >
            {drawerContent}
          </Drawer>
        </Hidden>
        <main className={this.classes.content}>
          <div className={this.classes.toolbar} />
          {this.props.user ? 
            <Switch>
              <Route path="/my_business/" component={Business} />
              <Route path="/my_workers/" component={Workers} />
              <Route path="/worker/:workerId/settings" component={DisplayWorkerSettings} />
              <Route path="/worker/:workerId/specials" component={DisplaySpecials} />
              <Route path="/worker/:workerId" component={DisplaySingleWorker}/>
              <Route path="/my_clients/" component={Clients} />
            </Switch> :
            <Switch>
              <Route path="/login/" component={Login} />
              <Route path="/register/" component={Register} />
            </Switch>}

        </main>
      </div>
    )
  }
}

DangBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => {
  return {
    user: state.user,
    token: state.token
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onUserLogout: () => dispatch({type: "LOGOUT"}),
    onUserUpdate: (user) => dispatch({type: "USER_UPDATE", user: user})
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(DangBar)));