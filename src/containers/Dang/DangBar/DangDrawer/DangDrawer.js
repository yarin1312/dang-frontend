import React, {Component} from 'react';
import PropTypes from 'prop-types';

import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  itemChoose: {
    backgroundColor: "lightgray",
    fontWeight: "bold"
  },
});

class DangDrawer extends Component {
  classes = this.props.classes;

  getDrawer() {
    return this.props.user ? this.getLoginDrawer() : this.getLogoutDrawer();
  }
  getLoginDrawer() {
    return (
      <List>
        <Divider />
        <ListItem className={window.location.pathname.indexOf('my_business') !== -1 ? this.classes.itemChoose : ""} 
                  button
                  onClick={() => this.props.history.push('/my_business/')}>
          <ListItemText primary="My Business" />
        </ListItem>
        <ListItem className={window.location.pathname.indexOf('workers') !== -1 ? this.classes.itemChoose : ""} 
                  button
                  onClick={() => this.props.history.push('/my_workers/')}>
          <ListItemText primary="Workers" />
        </ListItem>
        <ListItem className={window.location.pathname.indexOf('my_clients') !== -1 ? this.classes.itemChoose : ""} 
                  button
                  onClick={() => this.props.history.push('/my_clients/')}>
          <ListItemText primary="My Clients" />
        </ListItem>
        <Divider />
        <ListItem button>
          <ListItemText primary="Documentation" />
        </ListItem>
        <ListItem button>
          <ListItemText primary="About Us" />
        </ListItem>
      </List>
    )
  }
  getLogoutDrawer() {
    return (
      <List>
        <Divider />
        <ListItem button>
          <ListItemText primary="Start Now" onClick={() => this.props.history.push("/register/")}/>
        </ListItem>
        <ListItem button>
          <ListItemText primary="Documentation" />
        </ListItem>
        <ListItem button>
          <ListItemText primary="About Us" />
        </ListItem>
      </List>
    )
  }
  render() {
    const drawerToReturn = this.getDrawer();
    return drawerToReturn
  }
}

DangDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DangDrawer);