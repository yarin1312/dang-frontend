import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import Chip from '@material-ui/core/Chip';
import Divider from '@material-ui/core/Divider';
import {withStyles} from '@material-ui/core/styles';

import DangTextField from '../../../../components/DangFields/DangTextField';
import DangPasswordField from '../../../../components/DangFields/DangPasswordField';

const styles = theme => ({
  header: {
    paddingLeft: "10px",
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 300,
    backgroundColor: "white"
  },
  loginButton: {
    marginTop: "10px",
    paddingLeft: "10px",
  },
  chip: {
    margin: theme.spacing.unit,
  },
})

class Login extends Component {
  classes = this.props.classes;
  state = {
    email: {value: "", error: false},
    password: {value: "", error: false},
    loginFailed: false,
  }

  handleChange = field => (value, error) => {
    let entity = {...this.state[field]};
    entity.value = value
    entity.error = error

    this.setState({[field]: entity, loginFailed: false});
  }
  onLogin = (form) => {
    form.preventDefault();
    const dataToLogin = {
      email: this.state.email.value,
      password: this.state.password.value
    }

    axios.post('api/internal/user_login/', dataToLogin)
      .then( (res) => {
        this.props.onUserLogin({token: res.data.token, user: res.data.user});
        localStorage.setItem('user_id', res.data.user.id);
        localStorage.setItem('token', res.data.token);

        window.location = window.location.origin;
      })
      .catch( (err) => {
        this.setState({loginFailed: true});
      })
  }
  render () {
    const isValid = !this.state.email.error && !this.state.password.error;
    return (
      <div>
        <div className={this.classes.header}>
          <h2>Login</h2>
          <p><small>Not registered yet? Join dang now!</small></p>
          <p><small>Forget you'r password? Click here</small></p>
        </div>
        <Divider />
        <form onSubmit={this.onLogin}>
          <div>
            <DangTextField type="text"
                           value={this.state.email.value}
                           label="Email"
                           className={this.classes.textField}
                           handleChange={this.handleChange('email')}
                           required
                           />
            <DangPasswordField value={this.state.password.value}
                              label="Password"
                              className={this.classes.textField}
                              handleChange={this.handleChange('password')}
                              required/>
          </div>
          {this.state.loginFailed ? (
            <div>
              <Chip label="Login failed, please try again"
                    className={this.classes.chip}
                    color="secondary"/>
            </div>) : null}
          <div className={this.classes.loginButton}>
            <Button variant="contained"
                    type="sumbit"
                    color="primary"
                    size="large"
                    disabled={!isValid}>Login</Button>
          </div>
        </form>
      </div>)
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onUserLogin: () => dispatch({type: "LOGIN"})
  };
};

Login.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default connect(null, mapDispatchToProps)(withStyles(styles)(Login));