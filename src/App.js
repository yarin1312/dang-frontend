import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';

import Dang from './containers/Dang/Dang'

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Dang />
      </BrowserRouter>
    );
  }
}

export default App;
