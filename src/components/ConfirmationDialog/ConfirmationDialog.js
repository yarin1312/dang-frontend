import React from 'react';

import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';

const confirmationDialog = props => {
  return (
    <Dialog open={props.open}
            disableBackdropClick
            disableEscapeKeyDown
            maxWidth="xs"
            aria-labelledby="confirmation-dialog-title">
      <DialogTitle id="confirmation-dialog-title">
        Are you sure you want to {props.title} ?
      </DialogTitle>
      <DialogContent>{props.content}</DialogContent>
      <DialogActions>
        <Button color="secondary" onClick={props.onCancel}>Cancel</Button>
        <Button color="primary" onClick={props.onAccept}>Confirm</Button>
      </DialogActions>
    </Dialog>
  )
}

export default confirmationDialog;