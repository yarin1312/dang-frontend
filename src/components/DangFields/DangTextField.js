import React, {Component} from 'react'
import TextField from '@material-ui/core/TextField';

class DangTextField extends Component {
  state = {
    error: false,
    error_msg: "",
    value: ""
  }
  validateField = (newVal) => {
    if (this.props.required && !newVal) {
      return {error: true, error_msg: "This field is require"}
    }
    else if (this.props.validation) {
      return this.props.validation(newVal)
    }
    return {error: false, error_msg: ""}
  }
  onChangeValue = (event) => {
    const validation = this.validateField(event.target.value);
    this.props.handleChange(event.target.value, validation.error);
    this.setState({
      value: event.target.value,
      ...validation
    });
  }
  componentDidMount = () => {
    const validation = this.validateField(this.props.value);
    this.props.handleChange(this.props.value, validation.error);
    this.setState({
      value: this.props.value,
      ...validation
    });
  }
  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if (this.props.value !== this.state.value) {
      const validation = this.validateField(this.props.value);
      this.props.handleChange(this.props.value, validation.error);
      this.setState({
        value: this.props.value,
        ...validation
      });
    }
  }
  getHelperText = () => {
    return this.state.error ? this.state.error_msg : this.props.helperText;
  }

  render() {
    return (
      <TextField type={this.props.type}
                 error={this.state.error}
                 value={this.props.value}
                 onChange={this.onChangeValue}
                 label={this.props.label}
                 margin="normal"
                 className={this.props.className}
                 variant="outlined"
                 helperText={this.getHelperText()}
                 required={this.props.required}
                 inputProps={this.props.intputOtherProps}
                 {...this.props.otherProps}/>
    )
  }
}

export default DangTextField;