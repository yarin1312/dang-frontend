import React from 'react';

import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

import DangTextField from './DangTextField';

class DangPasswordField extends DangTextField {
  constructor (props) {
    super (props);
    this.state.showPassword = false;
  }

  render() {
    return (
      <TextField type={this.state.showPassword ? "text" : "password"}
                 error={this.state.error}
                 value={this.props.value}
                 onChange={this.onChangeValue}
                 label={this.props.label}
                 margin="normal"
                 className={this.props.className}
                 variant="outlined"
                 helperText={this.getHelperText()}
                 required={this.props.required}
                 InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="Toggle password visibility"
                        onClick={() => this.setState({showPassword: !this.state.showPassword})}
                      >
                        {this.state.showPassword ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                    ),
                  }}/>
    )
  }
}

export default DangPasswordField;