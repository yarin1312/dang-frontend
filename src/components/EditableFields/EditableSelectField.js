import React, {Component} from 'react'
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

const styles = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 300,
    backgroundColor: "white",
  },
  button: {
    margin: theme.spacing.unit,
  },
})

class EditableSelectField extends Component {
  classes = this.props.classes
  state = {
    value: ""
  }
  componentDidMount = () => {
    this.resetVal();
  }
  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if (prevProps.value !== this.props.value) {
      this.resetVal();
    }
  }
  resetVal = () => this.setState({value: this.props.value || ""})
  render() {
    const disable = (this.props.require && !this.state.value) || (this.props.value === this.state.value) || (!this.props.value && !this.state.value)
    return (
      <div>
        <div>
          <FormControl variant="filled" className={this.classes.textField}>
            <InputLabel htmlFor="editable">{this.props.label}</InputLabel>
            <Select value={this.state.value}
                    onChange={event => this.setState({value: event.target.value})}
                    input={<FilledInput name="editable" id="editable" />}>
              {this.props.elements.map((elem, index) =>
                <MenuItem key={index} value={elem.value}>{elem.text}</MenuItem>
              )}
            </Select>
          </FormControl>
        </div>
        <div>
          <Button className={this.classes.button}
                  variant="outlined"
                  color="primary"
                  disabled={disable}
                  onClick={() => this.props.onSave(this.state.value)}>Save</Button>
          <Button className={this.classes.button}
                  variant="outlined"
                  disabled={this.state.value === this.props.value || (!this.state.value && !this.props.value)}
                  onClick={() => this.setState({value: this.props.value || "", error: false})}>RESET</Button>
        </div>
      </div>
    )
  }
}

EditableSelectField.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(EditableSelectField);