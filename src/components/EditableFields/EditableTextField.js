import React, {Component} from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

import DangTextField from '../DangFields/DangTextField';

const styles = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 300,
    backgroundColor: "white",
  },
  button: {
    margin: theme.spacing.unit,
  },
})

class EditableTextField extends Component {
  classes = this.props.classes
  state = {
    value: "",
    error: false,
  }
  componentDidMount = () => {
    this.setState({value: this.props.value || "", error: false})
  }
  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if (prevProps.value !== this.props.value) {
      this.setState({
        value: this.props.value || "",
        error: false
      })
    }
  }
  render() {
    const disable = (this.props.require && !this.state.value) || (this.props.value === this.state.value) || (!this.props.value && !this.state.value)
    return (
      <div>
        <div>
          <DangTextField label={this.props.label}
                        className={this.classes.textField}
                        value={this.state.value}
                        otherProps={this.props.otherProps}
                        handleChange={(val, err) => this.setState({value: val, error: err})}
                        validation={this.props.validation}
                        required={this.props.required}
                        helperText={this.props.helperText}
                        type={this.props.type}
          />
        </div>
        <div>
          <Button className={this.classes.button}
                  variant="outlined"
                  color="primary"
                  disabled={disable}
                  onClick={() => this.props.onSave(this.state.value)}>Save</Button>
          <Button className={this.classes.button}
                  variant="outlined"
                  disabled={this.state.value === this.props.value || (!this.state.value && !this.props.value)}
                  onClick={() => this.setState({value: this.props.value || "", error: false})}>RESET</Button>
        </div>
      </div>
    )
  }
}

EditableTextField.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(EditableTextField);