const initialState = {
  user: null,
  token: null,
  worker: null
}

const reducer = (state=initialState, action) => {
  if (action.type === "LOGIN"){
    return {
      ...state,
      token: action.token,
      user: action.user
    };
  }
  if (action.type === "LOGOUT") {
    return {
      token: null,
      user: null
    }
  }
  if (action.type === "USER_UPDATE") {
    return {
      ...state,
      user: action.user,
    };
  }
  if (action.type === "SAVE_WORKER") {
    return {
      ...state,
      worker: action.worker,
    };
  }
  return state
}

export default reducer;
